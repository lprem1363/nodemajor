// make an express application
// attach port to that application
import express, { json } from "express";
import connectToMongoDB from "./src/connectToDb/connectToMongoDb.js";
import humidityRouter from "./src/router/humidityRouter.js";
import soilMoistureRouter from "./src/router/soilMoistureRouter.js";
import temperatureRouter from "./src/router/temperatureRouter.js";
import userRouter from "./src/router/userRouter.js";
// import fileRouter from "./src/router/fileRouter.js";
//option(alt) +shift+o ley used nabhako imprt haru hataidincha and packages ra file haru milayera rakhcha


let expressApp = express();
expressApp.use(json());
expressApp.use(express.static("./public"));


expressApp.listen(8000, () => {
  console.log("express application is listening at port 8000");
});



expressApp.use("/temperatures",temperatureRouter);
expressApp.use("/humiditys",humidityRouter);
expressApp.use("/soilMoistures",soilMoistureRouter);
expressApp.use("/users",userRouter);


connectToMongoDB()
// url = localhost:8000/product?address=gagalphedi
// url = route?query
// route = localhost:8000/product/a/b
// route = baserUrl/routeParameter1/routeParameter2

//GENERATE HASH PASSWORD
// let password="prem123"
// let hashPassword= await bcrypt.hash(password,10)
// console.log(hashPassword)


// let hashPassword="$2b$10$sFoiA3X0DIHg3MUJr4m9S.10EYJnRkiTmco7MLiBcEG9/V4eTn1l2"
// let password="prem123"
// let isValidPassword =await bcrypt.compare(password,hashPassword)
// console.log(isValidPassword)

//DYNAMIC ROUTE SHOULD BE ALWAYS AT LAST 
/* 
locallhost:8000/users/login
method post

*/
// let infoObj={
//   name:"prem",//id="1233234"
//   age:24
// }
// let secretKey="dw10"//string
// let  expiryInfo={
//   expiresIn:"365d"}

//   let token = jwt.sign(infoObj,secretKey,expiryInfo)
//   console.log(token)


// let token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoicHJlbSIsImFnZSI6MjQsImlhdCI6MTcwMjAyMTkzMywiZXhwIjoxNzMzNTU3OTMzfQ.msmXgILEAoK2Kk_Hx5hpbRKbgpopg0wi42RGNQ9Iwg8"
// try {
//   let infoObj=jwt.verify(token,"dw10")
//   console.log(infoObj)
// } catch (error) {
//   console.log(error.message)
// }

/* 
to be verified
token must be made from the given secretKey
must not expire
*/

let files = [
    {
      destination: "./public",
      filename: "abc.jpg",
    },
    {
      destination: "./public",
      filename: "nitan.jpg",
    },
    {
      destination: "./public",
      filename: "ram.jpg",
    },
  ];
  let link=files.map((value,i)=>{
    return(`http://localhost:8000/${value.filename}`)
  })
  console.log(link)

  
  
  // [
  //   "http://localhost:8000/abc.jpg",
  //   "http://localhost:8000/nitan.jpg",
  //   "http://localhost:8000/ram.jpg",
  // ]