import { model } from "mongoose";
import humiditySchema from "./humiditySchema.js";
import soilMoistureSchema from "./soilmoistureSchema.js";
import temperatureSchema from "./temperatureSchema.js";
import userSchema from "./userSchema.js";



export let Temperature = model("Temperature",temperatureSchema)
export let Humidity = model("Humidity",humiditySchema)
export let SoilMoisture = model("SoilMoisture",soilMoistureSchema)
export let User = model("User",userSchema)
