import { Schema } from "mongoose";

let soilMoistureSchema=Schema({//OBJECT IS DEFINED IN SCHEMA
    soilMoistureValue:{
        type:Number,
        required:[true,"soilMoisture field is required"]
    }
    
},
{
    timestamps:true
}
)
export default soilMoistureSchema
