import mongoose, { Schema } from "mongoose";

let userSchema = Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      minLength: 6,
    },
    // country: {
    //   type: String,
    //   required: true,
      
    // },
    // gender: {
    //   type:String,
    //   required: true,
    // },
    // description:{
    //    type :String,
    //     required:false,
    // },
   
  },
  {
    timestamps: true,
  }
);

export default userSchema;
