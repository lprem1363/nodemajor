import { Schema } from "mongoose";

let temperatureSchema=Schema({//OBJECT IS DEFINED IN SCHEMA
    temperatureValue:{
        type:Number,
        required:[true,"temperature  field is required"]
    }
    
},
{
    timestamps:true
}
)
export default temperatureSchema
