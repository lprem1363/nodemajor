import { Schema } from "mongoose";

let humiditySchema=Schema({//OBJECT IS DEFINED IN SCHEMA
    humidityValue:{
        type:Number,
        required:[true,"Humidity field is required"]
    }
    
},
{
    timestamps:true
}
)
export default humiditySchema
