import { SoilMoisture } from "../schema/model.js"


export let createSoilMoisture=async(req,res,next)=>{

    let soilMoistureData = req.body

    try {
        let result = await SoilMoisture.create(soilMoistureData)
        res.json({
            success: true,
            message:"SoilMoisture created successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readSoilMoisture=async(req,res,next)=>{


    try {
        // let result = await WebSoilMoisture.find({})
        let result = await SoilMoisture.find({}).select()

        res.json({
            success: true,
            message:"SoilMoisture read successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let deleteSoilMoisture=async(req,res,next)=>{

    let soilMoistureId = req.params.soilMoisturesId

    try {
        let result = await SoilMoisture.findByIdAndDelete(soilMoistureId)
        res.json({
            success: true,
            message:"SoilMoisture deleted successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readSoilMoistureDetails=async(req,res,next)=>{
    let soilMoistureId=req.params.soilMoisturesId
    try {
         let result=await SoilMoisture.findById(soilMoistureId)

        res.json({
            success:true,
            message:"SoilMoisture read by Id successfully",
            result:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
   


}
export let updateSoilMoisture=async(req,res,next)=>{
    let soilMoistureId=req.params.soilMoisturesId
    let soilMoistureData=req.body
    try {
        let result=await SoilMoisture.findByIdAndUpdate(soilMoistureId,soilMoistureData)
        res.json({
            success:true,
            message:"SoilMoisture updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
    }

   