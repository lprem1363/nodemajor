import { User } from "../schema/model.js"


export let createUser=async(req,res,next)=>{

    let userData = req.body

    try {
        let result = await User.create(userData)
        res.json({
            success: true,
            message:"user created successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let loginUser=async(req,res,next)=>{

    let userData = req.body

    try {
        let result = await User.create(userData)
        res.json({
            success: true,
            message:"user created successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readUser=async(req,res,next)=>{


    try {
        // let result = await WebTemperature.find({})
        let result = await User.find({})

        res.json({
            success: true,
            message:"user read successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let deleteUser=async(req,res,next)=>{

    let userId = req.params.TemperaturesId

    try {
        let result = await User.findByIdAndDelete(temperatureId)
        res.json({
            success: true,
            message:"Temperature deleted successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readUserDetails=async(req,res,next)=>{
    let userId=req.params.usersId
    try {
         let result=await User.findById(userId)

        res.json({
            success:true,
            message:"user read by Id successfully",
            result:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
   


}
export let updateUser=async(req,res,next)=>{
    let userId=req.params.usersId
    let userData=req.body
    try {
        let result=await User.findByIdAndUpdate(userId,userData)
        res.json({
            success:true,
            message:"user updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
    }

   