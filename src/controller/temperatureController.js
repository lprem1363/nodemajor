import { Temperature } from "../schema/model.js"


export let createTemperature=async(req,res,next)=>{

    let temperatureData = req.body

    try {
        let result = await Temperature.create(temperatureData)
        res.json({
            success: true,
            message:"Temperature created successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readTemperature=async(req,res,next)=>{


    try {
        // let result = await WebTemperature.find({})
        let result = await Temperature.find({}).select()

        res.json({
            success: true,
            message:"Temperature read successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let deleteTemperature=async(req,res,next)=>{

    let temperatureId = req.params.TemperaturesId

    try {
        let result = await Temperature.findByIdAndDelete(temperatureId)
        res.json({
            success: true,
            message:"Temperature deleted successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readTemperatureDetails=async(req,res,next)=>{
    let temperatureId=req.params.temperaturesId
    try {
         let result=await Temperature.findById(temperatureId)

        res.json({
            success:true,
            message:"Temperature read by Id successfully",
            result:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
   


}
export let updateTemperature=async(req,res,next)=>{
    let temperatureId=req.params.temperaturesId
    let temperatureData=req.body
    try {
        let result=await Temperature.findByIdAndUpdate(temperatureId,temperatureData)
        res.json({
            success:true,
            message:"Temperature updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
    }

   