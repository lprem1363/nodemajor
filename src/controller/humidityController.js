import { Humidity } from "../schema/model.js"


export let createHumidity=async(req,res,next)=>{

    let humidityData = req.body

    try {
        let result = await Humidity.create(humidityData)
        res.json({
            success: true,
            message:"Humidity created successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readHumidity=async(req,res,next)=>{


    try {
        // let result = await WebHumidity.find({})
        let result = await Humidity.find({}).select()

        res.json({
            success: true,
            message:"Humidity read successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let deleteHumidity=async(req,res,next)=>{

    let humidityId = req.params.humiditysId

    try {
        let result = await Humidity.findByIdAndDelete(humidityId)
        res.json({
            success: true,
            message:"Humidity deleted successfully",
            result:result
        })
        
    } catch (error) {

        res.json({
            success:false,
            message:error.message
        })
        
    }
    

}

export let readHumidityDetails=async(req,res,next)=>{
    let humidityId=req.params.humiditysId
    try {
         let result=await Humidity.findById(humidityId)

        res.json({
            success:true,
            message:"Humidity read by Id successfully",
            result:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
   


}
export let updateHumidity=async(req,res,next)=>{
    let humidityId=req.params.humiditysId
    let humidityData=req.body
    try {
        let result=await Humidity.findByIdAndUpdate(humidityId,humidityData)
        res.json({
            success:true,
            message:"Humidity updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
    }

   