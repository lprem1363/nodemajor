import { Router } from "express";
import { createTemperature, deleteTemperature, readTemperature, readTemperatureDetails, updateTemperature } from "../controller/temperatureController.js";

let temperatureRouter=Router()
temperatureRouter
.route("/")
.post(createTemperature)
.get(readTemperature)

temperatureRouter
.route("/:temperaturesId")
.delete(deleteTemperature)

.get(readTemperatureDetails)

.patch(updateTemperature)


export default temperatureRouter

