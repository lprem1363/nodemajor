import { Router } from "express";
import { createUser, deleteUser, loginUser, readUser, readUserDetails, updateUser } from "../controller/userController.js";


let userRouter = Router();
userRouter.route("/").get(readUser);
userRouter.route("/signup").post(createUser);
userRouter.route("/login").post(loginUser);
// userRouter.route("/booking/:userId").get(readUserBookingDetails);
userRouter
  .route("/:userId")
  .get(readUserDetails)
  .patch(updateUser)
  .delete(deleteUser);
export default userRouter;