import { Router } from "express";
import { createHumidity, deleteHumidity, readHumidity, readHumidityDetails, updateHumidity } from "../controller/humidityController.js";


let humidityRouter=Router()
humidityRouter
.route("/")
.post(createHumidity)
.get(readHumidity)

humidityRouter
.route("/:humiditysId")
.delete(deleteHumidity)

.get(readHumidityDetails)

.patch(updateHumidity)


export default humidityRouter

