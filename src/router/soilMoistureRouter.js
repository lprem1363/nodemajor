import { Router } from "express";
import { createSoilMoisture, deleteSoilMoisture, readSoilMoisture, readSoilMoistureDetails, updateSoilMoisture } from "../controller/soilMoistureController.js";


let soilMoistureRouter=Router() 
soilMoistureRouter
.route("/")
.post(createSoilMoisture)
.get(readSoilMoisture)

soilMoistureRouter
.route("/:soilMoisturesId")
.delete(deleteSoilMoisture)

.get(readSoilMoistureDetails)

.patch(updateSoilMoisture)


export default soilMoistureRouter

